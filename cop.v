(* COP, who, etc., modeled and best viewed in Coq 8.9 -- FreeBSD license *)

(*------------------SELECTIONAL RESTRICTIONS:-----------------*)

Module SR_ON.

Inductive SRH := Phy | Sen | Inf | Lim | Bio | Ani. (*humans*)
Existing Class SRH. Existing Instance Bio.

End SR_ON.

Module SR_OFF.

Inductive SRH := xx.
Notation Ani := xx.
Notation Phy := xx.
Notation Inf := xx.
Notation Lim := xx.
Notation Bio := xx.
Notation Sen := xx.
Existing Class SRH. Existing Instance xx.

End SR_OFF.

(*------------------OTHER TYPES:-----------------*)

Module Types.

(*SR switch: comment one out*)
Export SR_ON.
(* Export SR_OFF. *)

Inductive GN := Ml | Fm.  (*gender*)
Inductive ST := F | N | PN | PR | Nn (x:GN) | Pn (x:GN) | Pr (x:GN). (*stem: flexible, noun, proper name, pronoun*)
Inductive NUM := SG | PLR. (*number*)

Inductive SR := srh (x:SRH). (*general sr-s*)
Coercion hum_sr (x:SRH): SR := srh x.
Notation "' x" := (srh x) (at level 8). (*general sr-s*)
Notation "> x" := (hum_sr x) (at level 8). (*humans (coerced to general)*)
Definition back (x:SR): SRH := match x with srh x => x end. (*req-ed by Canon only*)
Notation "+ x" := (back x) (at level 7).

Check >Phy. (*tests*)
Check 'Phy.
Check +'+>Phy.

(*case/adposition: nominative, genitive, lative, directive, locative*)
Inductive CAS := NOM | ACC | GEN | LAT | DIR | LOC.
Definition ACC' := NOM. (*false accusative (so-called 0-derivation)*)
Structure ACC0 := acc0 {acc:CAS}.  (*NOM/ACC'/ACC*)
Canonical Structure acc_n := acc0 ACC'. (*NOM/ACC'*)
Canonical Structure acc_a := acc0 ACC.  (*ACC*)

End Types.

(*------------------MINIMAL:-----------------*)

Module Min. (*minimal standalone fragment w/ tests*)

    Inductive GN := Ml | Fm.  (*gender*)
    Inductive ST := F | N | PN | PR (*flexible, noun, proper name, pronoun*)
    | Nn (x:GN) | Pn (x:GN) | Pr (x:GN). (*noun, proper name, pronoun w/ gender*)
    Inductive NUM := SG | PLR. (*number*)

(*selectional restrictions*)
Inductive SR0 := Phy | Sen | Inf | Ani | Bio. (*all/humans*)
Inductive SRV := Phy_Ani. (*vehicles*)
(* Inductive SRV := v (x := Phy) | v0 (y := Ani) (* Phy_v | Ani_v *). (*vehicles*) *)
Inductive SR := sr (x:SR0) | hum_sr (x:SR0):> SR | veh_sr (x:SRV):> SR.

(*note: for completeness, some alternative/old solutions are included as comments*)

(* Notation "' x" := (sr x) (at level 8). (*general*)
Notation "> x" := (hum_sr x) (at level 8). (*humans (coerced to general)*)
Notation "'>v' x" := (veh_sr x) (at level 8). (*vehicles (coerced to general)*) *)

(*case/adposition: nominative, genitive, lative, directive, locative*)
Inductive CA := NOM | ACC | GEN | LAT | DIR | LOC.
Definition ACC' := NOM. (*false accusative (so-called 0-derivation)*)
Structure ACC0 := acc0 {acc:CA}.  (*NOM/ACC'|ACC*)
Canonical Structure acc_n := acc0 ACC'. (*NOM/ACC'*)
Canonical Structure acc_a := acc0 ACC.  (*ACC*)

Parameter NF S: Prop. (*nonfinite, sentence types*)
Parameter STM PLU XP: SR -> CA -> NUM -> ST -> Type.  (*stem, plural, XP*)

(* Inductive LUM := STM (x:SR)(y:CA)(z:NUM)(w:ST)
  | PLU (x:SR)(y:CA)(z:NUM)(w:ST)
  | XP (x:SR)(y:CA)(z:NUM)(w:ST). (*lump types*)
Parameter cc:> LUM -> Prop. *)

Structure CSTM := cs {gs: SR -> CA -> NUM -> ST -> (* LUM *) Type}.
Canonical Structure cs_s: CSTM := cs STM.
Canonical Structure cs_p: CSTM := cs PLU.

Structure CPX := cpx {gp: SR -> CA -> NUM -> ST -> (* LUM *) Type}.
Canonical Structure cp_p := cpx PLU.
Canonical Structure cp_x := cpx XP.

Definition ADJ {x y z w u} := gs x y z w u -> gs x y z w u.
Parameter ill: forall {x y z w}, @ADJ x Bio y z w.

Parameter hut: STM (sr Phy) (acc acc_n) SG N.
Parameter car: forall {x:SRV}, STM x (acc acc_n) SG N.
Parameter man: forall {x:SR0}, STM x (acc acc_n) SG N.
Parameter a: forall {x y z w}, gs x y z SG w -> gp cp_x y z SG w.
Parameter he: forall {x:SR0}, XP x NOM SG (Pr Ml).
Parameter him: forall {x:SR0}, XP x ACC SG (Pr Ml).
Parameter TAM: forall {x:Type}, (NF -> x) -> x.  (*tense-aspect-mood*)

Structure PHY := phy0 {phy:SR}.  (*physical entities*)
Canonical Structure phy_a := phy0 (sr Phy). (*default*)
Canonical Structure phy_b {x} := phy0 (hum_sr x).  (*human*)
Canonical Structure phy_c {x} := phy0 (veh_sr x).  (*vehicle*)

Structure ANI := ani0 {ani:SR}.  (*animate entities*)
Canonical Structure ani_a := ani0 (sr Ani). (*default*)
Canonical Structure ani_b {x} := ani0 (hum_sr x).  (*human*)
Canonical Structure ani_c {x} := ani0 (veh_sr x).  (*vehicle*)

Parameter red: forall {x y z w f}, @ADJ x (phy f) y z w.
Parameter hit: forall {w u k d m n f}, NF -> gp cp_x (ani f) NOM w u -> gp cp_x k (acc d) m n -> S.

Structure Sleep := mSleep {sleep':Type}. (*flexible*)
Canonical Structure sleep_s := mSleep (gs cs_s (sr Inf) NOM SG F). (*need gs cxs_s instead of STM; all 
events are Inf*)
Canonical Structure sleep_f {b v y} := mSleep (NF -> gp b Sen NOM v y -> S).
Parameter sleep: forall {b:Sleep}, sleep' b.

Check a sleep.
Fail Check TAM sleep man.
Check TAM sleep (a man).
Fail Check a (red sleep).

(* Parameter TAM': forall {x:Type}, (NF -> x) -> x.  (*tense-aspect-mood*)
Parameter red: forall {x y z w}, @ADJ x Phy y z w.
Parameter hit': forall {w u k d m n}, NF -> gp cp_x Ani NOM w u -> gp cp_x k (acc d) m n -> S.
Notation "'TAM' 'hit' x y" := ltac:(lazymatch type of x with
  | gp cp_x >_ (acc acc_n) _ _ => refine (TAM' hit' _ y)
  | XP >_ NOM _ _ => refine (TAM' hit' _ y)
  | gp cp_x 'Ani (acc acc_n) _ _ => refine (TAM' hit' _ y)
  |  _ => fail end) (at level 9, x, y at next level).
Existing Class SR0. Existing Instance Phy. (*default instance*) *)

Check TAM hit (a man) (a hut). (*tests*)
Fail Check TAM hit (a hut) (a man). (*wrong sel. res.*)
Fail Check TAM hit man (a man). (*"man" is not XP*)
Check TAM hit (a man) (a man).
Fail Check TAM hit (a man) he. (*wrong case*)
Check TAM hit (a man) him.
Check TAM hit he him.
Fail Check TAM hit him him. (*wrong case*)
Fail Check TAM hit (red he) him. (*"he" is not STM*)
Fail Check TAM hit (red man) him. (*"red man" is not XP*)
Check TAM hit (a (red man)) him.
Check TAM hit (a car) him.
Check a (red car).
Fail Check ill car.
Fail Check ill hut.
Check ill man.
Check red hut.

End Min.

(*------------------DEF-S:-----------------*)

Module Defs.
Export Types.

Definition S {x:SR} := Prop.  (*sentence*)
Parameter STM PLU XP: SR -> CAS -> NUM -> ST -> Type.  (*stem, plural, XP*)
Parameter hut: STM 'Phy (acc acc_n) SG N. (*need the guards ' for strict sr checking*)
Parameter man: forall {x:SRH}, STM x (acc acc_n) SG N.
Parameter i: forall {x:SRH}, XP x NOM SG PR.
Parameter me: forall {x:SRH}, XP x ACC SG PR.
Parameter thought: STM 'Inf (acc acc_n) SG N.

Check thought: STM Inf _ _ _. (*sr: tests*)
Check man: STM (hum_sr Phy) _ _ _.
Check man: STM 'Inf _ _ _.
Check man: STM >Inf _ _ _.
Check hut: STM >Phy _ _ _.

Structure CSTM := cs {gs: SR -> CAS -> NUM -> ST -> Type}.
Canonical Structure cs_s: CSTM := cs STM.
Canonical Structure cs_p: CSTM := cs PLU.

Structure CPX := cpx {gp: SR -> CAS -> NUM -> ST -> Type}.
Canonical Structure cp_p := cpx PLU.
Canonical Structure cp_x := cpx XP.

Definition ADJ {x y z w u} := gs x y z w u -> gs x y z w u.

Parameter NF: Prop.   (*nonfinite marker*)
Parameter red: forall {x y z w}, @ADJ x Phy y z w.
Parameter ill: forall {x y z w}, @ADJ x Bio y z w.
Parameter limbed: forall {x y z w}, @ADJ x Lim y z w.
Parameter a: forall {x y z w}, gs x y z SG w -> gp cp_x y z SG w.
Parameter few: forall {y z w}, gp cp_p y z PLR w -> gs cs_p y z SG w. (*out: "PLU..SG.." for "a few.."*)
Parameter several: forall {y z w}, gp cp_p y z PLR w -> gp cp_p y z PLR w.
Parameter the: forall {x y z u w}, gs x y z u w -> gp cp_x y z u w.
Parameter this: forall {x d w}, gs cs_s x d SG w -> gp cp_x x d SG w.
Parameter these: forall {x d b w}, gs cs_p x d b w -> gp cp_p x d PLR w. (*in: "PLU.." for "few.."*)

Parameter PL: forall {y z w}, gs cs_s y z SG w -> gp cp_p y z PLR w.
Parameter leave: forall {x w u}, NF -> gp x Ani NOM w u -> @S Ani.
Parameter know: forall {x y w u k d m n}, NF -> gp x Sen NOM w u -> gp y k (acc d) m n -> @S Sen.

Parameter who: forall {x z w u}, @S x -> XP Sen z w u.
Notation "'who' x" := ltac:(match type of x with | @S >_ => exact (@who _ _ _ _ x) | _ => fail end) (at level 9, x at next level).

Structure Sleep := mSleep {sleep':Type}. (*flexible*)
Canonical Structure sleep_s := mSleep (gs cs_s Inf NOM SG F). (*need gs cxs_s instead of STM; all 
events are Inf*)
Canonical Structure sleep_f {b v y} := mSleep (NF -> gp b Sen NOM v y -> @S Sen).
Parameter sleep: forall {b:Sleep}, sleep' b.

(*Definition PAST PRES {x y d}(t: x -> y -> @S d) := id t. (*makes † succeed*)*)
Parameter PAST PRES TAM: forall {x:Type}, (NF -> x) -> x.  (*tense-aspect-mood*)
Notation "'PAST'" := PAST.  (*notations trigger where parameters won't (e.g. ‡)*)
Notation "'PRES'" := PRES.

(*universal multicategories*)
Notation "'TAM'" := TAM.
Notation Q := (_: gp cp_p _ _ PLR _ -> gs cs_p _ _ _ _).
Parameter D: forall {x y z u w}, gs x y z u w -> gp cp_x y z u w. (*a universal decl-ed as a variable*)
Notation D' := (_: gs _ _ _ _ _ -> gp cp_x _ _ _ _). (*a universal def-ed as a notation*)
Notation CA := (_:CAS) (only parsing).
Notation X := (_:ST) (only parsing).

End Defs.

(*------------------CANON:-----------------*)

Module Canon. (*a canonical structure based version*)
Export Defs.

Structure COP' := cop' {cp:Type}.
Canonical Structure cop_a {x y z w u} := cop' (@ADJ x y z w u).
Canonical Structure cop_x {x y z w u} := cop' (gp x y z w u).

Parameter cop: forall {x y z}, cp x -> cp y -> @S z.
Notation "'COP'" := ltac:(refine (@cop (@cop_a _ ?[f] _ _ _) (@cop_x _ ?f (acc _) _ _) ?f)) (at level 9, only parsing).  (*let "COP" type-check*)

Notation "'TAM' 'COP' x y" :=
ltac:(exact (@cop (@cop_a _ ?[f] _ _ _) (@cop_a _ ?f _ _ _) '+?f x y) (*A->A->S*)
|| exact (@cop (@cop_a _ ?[f] _ _ _) (@cop_x _ ?f (acc _) _ _) ?f x y) (*A->X->S*)
|| lazymatch type of y with | gp _ >_ _ _ _ => (*A->>X->S: fail "is ill (a hut)"*)
exact (@cop (@cop_a _ ?[f] _ _ _) (@cop_x _ >_ (acc _) _ _) ?f x y) | _ => fail end
|| lazymatch type of x with  (*X->X->S: fail "is i me"*)
  | gp ?k ?l _ _ _ => lazymatch type of y with  (*we're copying the AXIOM notation:*)
    | gp k l _ _ _ =>                (*here gp.. *doesn't* match i's type, XP.. ¥*)
      exact (@cop (@cop_x ?[e] ?[f] _ _ _) (@cop_x ?e ?f (acc _) _ _) ?f x y)
    | _ => idtac 1; fail end
  | _ => idtac 2; fail end) (at level 9, x, y at next level).

End Canon.

Module Canon0. (*another canonical structure based version*)
Export Defs.

Structure KUNGLA := ku {gu: Type}.
Canonical Structure ku_aa  {x y z w u k l m n d} := ku (@ADJ x y z w u -> @ADJ k y l m n -> @S 'd).
Canonical Structure ku_ax {x y z w u k l m n} := ku (@ADJ x y z w u -> gp k y (acc l) m n -> @S y).
Canonical Structure ku_fx {x y z w u k l m n d} := ku (@ADJ x y z w u -> gp k >d (acc l) m n -> @S y).
Canonical Structure ku_xx {x y z w u l m n} := ku (gp x y z w u -> gp x y (acc l) m n -> @S y).
(*note: the warnings are ok, nothing's gained by making the proj-s non-redundant*)
Parameter COP': forall x, gu x.

Fail Check COP' _ ill (a man).  (*COP: type inf not working, so we..*)
Check COP' ku_ax ill (a man).   (*..need (sth like) the notations below*)

Notation "'COP'" := (COP' ku_ax) (at level 9, only parsing). (*let "COP" type-check*)
Notation "'TAM' 'COP' x y" := ltac:(exact (COP' ku_aa x y) || exact (COP' ku_ax x y) || match type of y with | gp _ >_ _ _ _ => exact (COP' ku_fx x y) | _ => fail end || lazymatch type of x with
  | gp ?k ?l _ _ _ => lazymatch type of y with  (*we're copying the AXIOM notation:*)
    | gp k l _ _ _ => exact (COP' ku_xx x y)    (*here gp.. *doesn't* match i's type, XP.. ¥*)
    | _ => idtac 1; fail end
  | _ => idtac 2; fail end) (at level 9, x, y at next level).

End Canon0.

(*------------------AXIOM:-----------------*)

Module Axio. (*an axiom-based version*)
Export Defs.

Parameter aa: forall {x y z w u k l m n d}, @ADJ x y z w u -> @ADJ k y l m n -> @S 'd.
Parameter ax: forall {x y z w u k l m n}, @ADJ x y z w u -> gp k y (acc l) m n -> @S y.
Parameter fx: forall {x y z w u k l m n d}, @ADJ x y z w u -> gp k >d (acc l) m n -> @S y.
Parameter xx: forall {x y z w u l m n}, gp x y z w u -> gp x y (acc l) m n -> @S y.

Notation "'COP'" := (@ax _ _ _ _ _ _ _ _ _) (at level 9). (*let "COP" type-check*)
Notation "'TAM' 'COP' x y" := ltac:(lazymatch type of x with
  | @ADJ _ ?i _ _ _ => lazymatch type of y with
    | @ADJ _ i _ _ _ => exact (aa x y)
    | gp _ i _ _ _ => exact (ax x y)
    | gp _ >_ _ _ _ => exact (fx x y) (*catches arg-s w/ multiple sr-s†*)
    | _ => fail end
  | gp ?j ?i _ _ _ => lazymatch type of y with
    | gp j i _ _ _ => exact (xx x y)
    | _ => fail end
  | _ => fail end) (at level 9, x, y at next level).

(*† "TAM COP red (a man)" failed w/ | gp _ _ _ _ _ => let u := constr:(y: gp _ i _ _ _)) in refine (ax x u)*)

End Axio.

(*------------------TEST:------------------*)

Module Test.
(* Import Axio. (*comment two imports out*) *)
Import Canon.
(* Import Canon0. *)

(* Existing Class SRH. Existing Instance xx. (*default instances for type inference*) *)
Existing Class CSTM. Existing Instance cs_s.
(* Parameter CA:CAS. *) Existing Class CAS. Existing Instance NOM.
Existing Class NUM. Existing Instance SG.
Existing Class ST. Existing Instance N.

Notation "'PRES' 'COP' x y" := (TAM COP x y) (at level 9, x, y at next level, only parsing).
Notation "'PAST' 'COP' x y" := (TAM COP x y) (at level 9, x, y at next level, only parsing).

Notation "'is' x y" := ltac:(first [exact (PRES COP x (y:gp cp_x _ _ SG _)) | exact (PRES COP (x:ADJ) (y:ADJ))]) (at level 9, x, y at next level, only parsing).
Notation "'are' x y" := ltac:(exact (PRES COP x (y:gp _ _ _ PLR _))) (at level 9, x, y at next level, only parsing).

Check COP: _ -> _.
Check a thought.
Check a (red hut).
Check the (ill man).
Check the (limbed man).
Check the (red (red man)).
Fail Check red (limbed man). (*†*)
Fail Check red (ill man).    (*† limitation of cop.v, solved w/ "[...]" in frag.v*)
Fail Check a (ill hut).
Check PAST COP ill (a man).
Fail Check COP ill red.
Check PRES COP (a hut) (a hut).
Check TAM COP ill ill.
Check TAM COP red red.
Fail Check TAM COP (a hut) (a man).
Check TAM COP (a hut) (a hut).
Check PAST COP ill (a man).
Check TAM COP ill (PL man).
Check TAM COP red (a man).
Fail Check TAM COP ill (a thought).
Fail Check TAM COP ill (a hut).
Fail Check PRES COP (PL man) (a man).
Fail Check is (a man) (PL man).
Fail Check COP ill ill.
Fail Check COP ill (a hut).
Check COP red (a hut).
Fail Check COP (a man) (a hut).
Fail Check are (PL man) (PL hut). (*"men are huts" won't do*)
Fail Check are (PL hut) (PL men). (*huts are men*)
Check PRES COP (PL man) (PL man). (*men are men*)

Check is ill ill.
Fail Check is ill (a hut).
Check is red (a man).
Check is (a hut) (a hut).
Fail Check are ill ill.
Check PAST COP ill (a man).
Check are (PL man) (PL man).
Fail Check are ill (the man).

Fail Check who (is ill ill).
Check who (is ill (a man)).
Fail Check who (COP (a hut) (a hut)).
Check who (PRES COP ill (a man)).
Check who (are ill (PL man)).
Check who (is red (a man)).

Fail Check is i me.  (*fails as desired (because gp.. doesn't match XP..) (see ¥)*)
Fail Check PRES know i (who (is ill ill)). (*"i know ill who is ill" won't do*)
Check TAM know i (who (are ill (PL man))).  (*i know men who are ill*)
Check the (few (PL man)): XP _ CA _ X.
Check PRES know i (who (PAST COP ill (the man))).  (*i know the man who is ill*)
Check TAM know i (who (are ill (the (PL man)))).  (*i know the men who are ill*)
Fail Check TAM know i (who (are ill (a (PL man)))).  (*"i know a men who are ill" won't do*)
Check PAST know i (who (TAM COP ill (a man))).  (*‡*)
Fail Check PRES PRES know i me.
Fail Check PAST (PAST know) i me.  (*†*)
Check PAST know i (the hut).
Check TAM know (a man) (the hut).
Check TAM know (PL man) (the (PL hut)).

Fail Check PRES know i the.
Fail Check PRES know i i.
Fail Check know i me.   (*TAM req-ed, know is nonfinite*)
Check PAST know i me.
Check PRES know i me.
Check TAM know i (who (is (the man) me)). (*i know the man who is me*)
Fail Check PRES know i (who (is (the man) i)).  (*wrong case*)
Check TAM know i (who (PAST COP ill (the man))).
Check PAST know i (who (are ill (PL man))).

Check few (PL man).
Check PAST leave (few (PL man)). (*few men left*)
Check PRES leave (a (few (PL man))).
Check the (few (PL man)).
Fail Check the (few man).
Fail Check few (few man).
Fail Check few (a man).
Check PRES know (several (PL man)) (a (few (PL man))). (*several men know a few men*)
Check TAM know (D' man) (D (Q (PL man))). (*e.g. "the man knows a few men"*)
Fail Check several (a man).
Fail Check several man.
Check PRES know (these (several (PL man))) (these (few (PL man))). (*these several men know these few men*)
Fail Check this (few (PL man)). (*this few men*)

Check sleep: NF -> _ -> S. (*typed as function*)
Check sleep: gs _ _ _ _ _. (*typed as argument*)
Fail Check PAST sleep man. (*fails since "man" is not an XP*)
Check PAST sleep (a man). (*a man slept*)
Check PAST sleep (few (PL man)). (*few men slept*)
Check PAST sleep (a (few (PL man))). (*a few men slept*)
Check a sleep.
Fail Check PAST sleep (a hut). (*a hut slept*)
Fail Check PAST sleep (a sleep). (*a sleep slept*)
Check TAM sleep (D (Q (PL man))). (*e.g. "a few men slept"*)
Check TAM sleep (D' (Q (PL man))). (*e.g. "a few men slept"*)

End Test.
