(* Compound types for NL modeling, written and best viewed in Coq 8.9 -- FreeBSD license *)

Module Types.

Inductive ST := F | N | PN. (*stem: flexible, noun, proper name*)
Inductive NUM := SG | PLR. (*number: singular, plural*)
Inductive SR := Phy | Sen | Inf | Lim. (*selectional restriction: physical, sentient, informational, limbed*)
Inductive CA := NOM | GEN | LAT | DIR | LOC. (*case/adposition: nominative, genitive, lative, directive, locative*)

End Types.

(*-----------application:------------*)

Module App.
Export Types.

Parameter STM PLU XP: SR -> CA -> NUM -> ST -> Type.
Parameter hut: STM Phy NOM SG N.
Parameter thought: STM Inf NOM SG N.

Structure CSTM := cx {g: SR -> CA -> NUM -> ST -> Type}.
Canonical Structure cx_x: CSTM := cx STM.
Canonical Structure cx_x': CSTM := cx PLU.

Parameter red: forall {x y z w}, g x Phy y z w -> g x Phy y z w.
Parameter a: forall {x y z w}, g x y z SG w -> XP y z SG w.
Parameter the: forall {x y z u w}, g x y z u w -> XP y z u w.
Parameter PL: forall {x y z w}, g x y z SG w -> PLU y z PLR w.

End App.

(*-----------record:------------*)

Module Rec.
Export Types.

Inductive CAT := STM | PLU | XP. (*stem, plural, XP*)
Record POS := mkPOS { cw:CAT; cw0:SR; cw1:CA; cw2:NUM; cw3:ST }.
Parameter co:> POS -> Set.
Parameter hut: mkPOS STM Phy NOM SG N.
Parameter thought: mkPOS STM Inf NOM SG N.

Structure CSTM := cx {g: CAT}.
Canonical Structure cx_x: CSTM := cx STM.
Canonical Structure cx_x': CSTM := cx PLU.

Parameter red: forall {x y z w}, mkPOS (g x) Phy y z w -> mkPOS (g x) Phy y z w.
Parameter a: forall {x y z w}, mkPOS (g x) y z SG w -> mkPOS XP y z SG w.
Parameter the: forall {x y z u w}, mkPOS (g x) y z u w -> mkPOS XP y z u w.
Parameter PL: forall {x y z w}, mkPOS (g x) y z SG w -> mkPOS PLU y z PLR w.

End Rec.

(*------------product:-----------*)

Module Prod.
Export Types.

Inductive CAT := STM | PLU | XP.
Open Scope type.
Definition POS := CAT*SR*CA*NUM*ST.
Parameter co:> POS -> Set.
Definition pos x y z w u := (x, y, z, w, u): POS.
Parameter hut: pos STM Phy NOM SG N.
Parameter thought: pos STM Inf NOM SG N.

Structure CSTM := cx {g: CAT}.
Canonical Structure cx_x: CSTM := cx STM.
Canonical Structure cx_x': CSTM := cx PLU.

Parameter red: forall {x y z w}, pos (g x) Phy y z w -> pos (g x) Phy y z w.
Parameter a: forall {x y z w}, pos (g x) y z SG w -> pos XP y z SG w.
Parameter the: forall {x y z u w}, pos (g x) y z u w -> pos XP y z u w.
Parameter PL: forall {x y z w}, pos (g x) y z SG w -> pos PLU y z PLR w.

End Prod.

(*------------$\Pi$-type:-----------*)

Module Pi.
Export Types.

Inductive CAT := STM | PLU | XP.
Parameter POS: forall (cw:CAT)(cw0:SR)(cw1:CA)(cw2:NUM)(cw3:ST), Type.
Parameter hut: POS STM Phy NOM SG N.
Parameter thought: POS STM Inf NOM SG N.

Structure CSTM := cx {g: CAT}.
Canonical Structure cx_x: CSTM := cx STM.
Canonical Structure cx_x': CSTM := cx PLU.

Parameter red: forall {x y z w}, POS (g x) Phy y z w -> POS (g x) Phy y z w.
Parameter a: forall {x y z w}, POS (g x) y z SG w -> POS XP y z SG w.
Parameter the: forall {x y z u w}, POS (g x) y z u w -> POS XP y z u w.
Parameter PL: forall {x y z w}, POS (g x) y z SG w -> POS PLU y z PLR w.

End Pi.

(*-----------tests:------------*)

Module Test. (*comment out imports as desired*)
Import (* App *) (* Rec *) Prod (* Pi *).

Check red hut.
Check a thought.
Fail Check red thought.
Check a (red hut).
Check the (red hut).
Check the (red (PL hut)).
Fail Check red (a hut).
Fail Check a (red (PL hut)).
Fail Check a (the (red hut)).
Fail Check the a.

End Test.