(* A fragment of NL structure in NLC, modeled and best viewed in Coq 8.9 -- FreeBSD license *)

(*-----------------CAT-s:-----------------*)

Inductive NUM := SG | PLR.  (*number*)
Inductive GN := Ml | Fm.  (*gender*)
(*argument stems: flexible, noun, proper name, pronoun:*)
Inductive ST := F | N | PN | PR | Nn (x:GN) | Pn (x:GN) | Pr (x:GN).
Notation "N+" := (Nn _) (only parsing). (*noun w/ gender info*)
Notation "PN+" := (Pn _) (only parsing).
Notation "PR+" := (Pr _) (only parsing). (*unused*)
Parameter NF S0 S1 S2: Prop.  (*nonfinite, sentence types*)
Inductive SR := Phy | Sen | Inf | Lim | sr0 (x y: SR). (*selectional restrictions*)
Inductive CA := NOM | ACC | GEN | LAT | DIR | LOC. (*case/adposition: nominative, genitive, lative, directive, locative*)
Definition ACC' := NOM. (*false accusative (so-called 0-derivation)*)
Structure ACC0 := acc0 {acc:CA}.  (*NOM/ACC'/ACC*)
Canonical Structure acc_n := acc0 ACC'. (*NOM/ACC'*)
Canonical Structure acc_a := acc0 ACC.  (*ACC*)

(*phrase architecture*)
Parameter STM: SR -> CA -> NUM -> ST -> Set.  (*stem*)
Parameter PLU: SR -> CA -> NUM -> ST -> Set.  (*PL*)
Parameter XP0: SR -> CA -> NUM -> ST -> Set.  (*PN*)
Parameter XP1: SR -> CA -> NUM -> ST -> Set.  (*a ...*)
Parameter XP2: SR -> CA -> NUM -> ST -> Set.  (*the ...*)
Parameter QU0: SR -> CA -> NUM -> ST -> Type. (*entire (SG) ...*)
Parameter QU1: SR -> CA -> NUM -> ST -> Type. (*entire (PL) ...*)
Parameter QU2: SR -> CA -> NUM -> ST -> Type. (*all, every ...*)


(*-------------------SUBTYPES†:-------------------*)

Structure CXA := cxa {ga: SR -> CA -> NUM -> ST -> Type}. (*all arg-s (for sr conv func FF*)
Canonical Structure cxa_s := cxa STM.
Canonical Structure cxa_p := cxa PLU.
Canonical Structure cxa_0 := cxa XP0.
Canonical Structure cxa_1 := cxa XP1.
Canonical Structure cxa_2 := cxa XP2.
Canonical Structure cxa_q0 := cxa QU0.
Canonical Structure cxa_q1 := cxa QU1.
Canonical Structure cxa_q2 := cxa QU2.

Structure CXD := cxd {gd: SR -> CA -> NUM -> ST -> Set}.  (*(in|out)put for ADJ, input to PL..*)
Canonical Structure cxd_s := cxd STM.
Canonical Structure cxd_p := cxd PLU.
Canonical Structure cxd_0 := cxd XP0.

Structure CXS := cxs {gs: SR -> CA -> NUM -> ST -> Type}.  (*input to a, the..*)
Canonical Structure cxs_s := cxs STM.
Canonical Structure cxs_p := cxs PLU.
Canonical Structure cxs_0 := cxs XP0.
Canonical Structure cxs_q := cxs QU0.

Structure CXP := cxp {gp: SR -> CA -> NUM -> ST -> Type}. (*input to V, F, CON..*)
Canonical Structure cxp_p := cxp PLU.
Canonical Structure cxp_0 := cxp XP0.
Canonical Structure cxp_1 := cxp XP1.
Canonical Structure cxp_2 := cxp XP2.
Canonical Structure cxp_q1 := cxp QU1.
Canonical Structure cxp_q2 := cxp QU2.

Structure CXQA := cxqa {gqa: SR -> CA -> NUM -> ST -> Type}.  (*input to all*)
Canonical Structure cxqa_p := cxqa PLU.
Canonical Structure cxqa_2 := cxqa XP2.

Structure CXQY := cxqy {gqy: SR -> CA -> NUM -> ST -> Type}.  (*input to (ever|an)y*)
Canonical Structure cxqy_s := cxqy STM.
Canonical Structure cxqy_0 := cxqy XP0.

Structure CXQT := cxqt {gqt: SR -> CA -> NUM -> ST -> Set}.  (*input to entire*)
Canonical Structure cxqt_s := cxqt STM.
Canonical Structure cxqt_p := cxqt PLU.

(* † of SR -> CA -> NUM -> ST -> Type *)


(*-------------------------------ARG-s (stems):----------------------------*)

(*--------semantically simple:------------*)

Parameter ball: STM Phy (acc acc_n) SG N. (*must use (acc acc_n) instead of NOM bec of the false acc sys*)
Parameter thought: STM Inf (acc acc_n) SG N.
Parameter hut: STM Phy (acc acc_n) SG N.

(*--------semantically complex:------------*)

(*note: the Notations below depend on this section: update them if you change it*)

(*physical-informational entities*)
Structure PhyInf := mk_PhyInf {pi: SR}.
Canonical Structure PhyInf_Phy := mk_PhyInf Phy.
Canonical Structure PhyInf_Inf := mk_PhyInf Inf.
Parameter phyinf: forall {x}, STM (pi x) (acc acc_n) SG N.
Existing Class PhyInf. Existing Instance PhyInf_Phy. (*default instance for type inference*)

Check ltac:(tryif unify Inf (pi _) then idtac "ja" else idtac "ei"). (*test/ltac ex.*) (*ei*)
Check ltac:(tryif let p := constr:(eq_refl: Inf = pi _) in exact p then idtac "ja" else idtac "ei"). (*ja*)

(*physical-informational-sentient-limbed entities*)
Structure Human := mk_H {hu: SR}.
Canonical Structure H_Phy := mk_H Phy.
Canonical Structure H_Lim := mk_H Lim.
Canonical Structure H_Sen := mk_H Sen.
Canonical Structure H_Inf := mk_H Inf.
Parameter human: forall {x}, gp cxp_0 (hu x) (acc acc_n) SG N. (*note: not used*)
Parameter john': forall {x}, gp cxp_0 (hu x) (acc acc_n) SG (Pn Ml). (*cannot use XP0 (bec of CON)*)
Parameter boy': forall {x}, gs cxs_s (hu x) (acc acc_n) SG (Nn Ml).
Parameter he': forall {x: Human}, gp cxp_2 (hu x) NOM SG (Pr Ml).
Parameter him': forall {x: Human}, gp cxp_2 (hu x) ACC SG (Pr Ml).
Existing Class Human. Existing Instance H_Phy. (*type inf: default instance*)

Check boy': STM _ ACC' SG _.  (*tests*)
Fail Check boy': STM _ ACC SG _.
Fail Check him': XP2 _ NOM SG _.
Fail Check he': XP2 _ ACC SG _.
Fail Check john': XP0 _ ACC' SG PN.
Check john': XP0 _ ACC' SG PN+.

(*-----sr conversion func:------*)

Parameter FF: forall r {z s d x y}, ga z s d x y -> ga z r d x y.

(*------type inf (for complex arg-s):-------*)

Notation pi_brac_hook := ltac:(first [exact (@FF (pi _) _ _ _ _ _ x) | exact (@FF (sr0 (pi _) (pi _)) _ _ _ _ _ x)]).
Notation hu_brac_hook := ltac:(first [exact (@FF (hu _) _ _ _ _ _ x) | exact (@FF (sr0 (hu _) (hu _)) _ _ _ _ _ x)]).
(*must use x to hook Notation "[ x ]"*)

(*-------------------------Notations (for complex arg-s):-----------------------*)

(*our only physical-informational entity is "book"*)
Notation book := phyinf (only parsing).

(*gs or gp (input for the next str)*)
Structure GX := gx' {gx:  SR -> CA -> NUM -> ST -> Type}.
Canonical Structure gx_s x := gx' (gs x). (*gs*)
Canonical Structure gx_p x := gx' (gp x). (*gp*)
Compute gx (gx_p cxp_0).
Compute gx (gx_s _).

(*we need these str-s for the generality of the Notation below*)
Structure HMN := hmn' {gs_gp; human'; ca'; num'; st'; hmn: (gx gs_gp) (hu human') ca' num' st'}.
Canonical Structure hmn_jh {x} := hmn' _ x _ _ _ john'.
Canonical Structure hmn_he {x} := hmn' _ x _ _ _ he'.
Canonical Structure hmn_hm {x} := hmn' _ x _ _ _ him'.
Canonical Structure hmn_by {x} := hmn' _ x _ _ _ boy'.
Notation john := (hmn hmn_jh).
Notation he := (hmn hmn_he).
Notation boy := (hmn hmn_by).
Notation him := (hmn hmn_hm).

Compute hmn hmn_he: XP2 Sen _ _ _.  (*tests*)
Compute hmn hmn_jh: XP0 Sen _ _ _.
Compute he: XP2 Sen _ _ _.
Compute him: XP2 Sen _ _ _.
Compute john: XP0 Sen _ _ _.

Module Lax.
Notation "[ x ]" := ltac:(first [exact (FF _ x) | exact x]) (at level 9).
End Lax.

Module Strict.
Notation "[ x ]" := ltac:(match x with
  | phyinf => exact pi_brac_hook
  | _ phyinf => exact pi_brac_hook
  | _ (_ phyinf) => exact pi_brac_hook
  | _ (_ (_ phyinf)) => exact pi_brac_hook
  | _ (_ (_ (_ phyinf))) => exact pi_brac_hook
  | _ (_ (_ (_ (_ phyinf)))) => exact pi_brac_hook
  | hmn _ => exact hu_brac_hook
  | _ (hmn _) => exact hu_brac_hook
  | _ (_ (hmn _)) => exact hu_brac_hook
  | _ (_ (_ (hmn _))) => exact hu_brac_hook
  | _ (_ (_ (_ (hmn _)))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (hmn _))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (hmn _)))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (_ (hmn _))))))) => exact hu_brac_hook
  | _ (_ (_ (_ (_ (_ (_ (_ (hmn _)))))))) => exact hu_brac_hook
  | ?p => exact p (*relax notation*)
  | _ => idtac "[...] fail"; fail end) (at level 9).
End Strict.

(*Lax/Strict switch (comment at most one out)*)
Import Lax. (*higher priority if both on*)
Import Strict.

Compute [hmn hmn_he]: XP2 Sen _ _ _.  (*tests*)
Compute [hmn hmn_jh]: XP0 Inf _ _ _.
Compute [he]: XP2 Sen _ _ _.
Compute [boy]: STM Lim _ _ _.
Check book: STM Inf NOM SG N.
Check book: STM Phy ACC' SG N.
Fail Check book: STM Lim NOM SG N.
Check john: XP0 Lim NOM SG (Pn Ml).
Compute [john]: XP0 Lim NOM SG (Pn Ml).
Check FF _ john: XP0 Lim NOM SG (Pn Ml).
Check boy: STM _ _ _  N+.
Fail Check book: STM _ _ _ N+.
Check [book].
Check [hut].


(*-------------------CASE/ADPOSITION:-------------------*)

(*simple*)
Parameter in': forall {z:CXP}{s:SR}{x:NUM}{y}, gp z s NOM x y -> S0 -> S0.
Check in' john.
Fail Check in' ball.
Check in' john ((in' john) _:S0): S0.
Notation "'in'" := in'.

(*simple w/ subtyping*)
Definition XP2DIR {s:SR}{x:NUM}{y} := XP2 s DIR x y.
Parameter xp'd: forall {s:SR}{x:NUM}{y}, @XP2DIR s x y. (*let to(wards) land on this type, and coerce it to ..LAT..*)
Parameter towards to: forall {z:CXP}{s:SR}{x:NUM}{w y}, gp z s (acc w) x y -> @XP2DIR s x y. (*to takes ACC not NOM*)
Parameter xp'd_lat:> forall {s:SR}{x:NUM}{y}, @XP2DIR s x y -> XP2 s LAT x y.

Check to _: XP2 _ DIR _ _.  (*tests*)
Check to _: XP2 _ LAT _ _.

(*poly(sem|morph)ic*)

(*generic (a Phy ver would have Phy instead of {s:SR})*)
Definition At {s x y} := ltac:(first [exact (XP2 s LAT x y) | exact (S0 -> S0)]).
Parameter at': forall {z s w x y}, gp z s (acc w) x y -> @At s x y. (*at takes ACC not NOM*)
Parameter at_lat:> forall {s x y}, @At s x y -> XP2 s LAT x y.
Definition S0S0 := S0 -> S0.
Parameter at_soso:> forall {s x y}, @At s x y -> S0S0.
Notation "'at'" := at'.
Identity Coercion ic: S0S0 >-> Funclass.

Check at john: XP2 _ LAT _ _.  (*tests*)
Check at john: S0 -> S0.
Fail Check at john: S0.
Check at john (_:S0).
Check at john ((at john) _:S0): S0. (*bec there's no anaphora res*)
Check in john ((in john) _:S0): S0.
Fail Check in book.


(*-------------------ADJECTIVE:-------------------*)

Definition ADJ {x d z y}(s:SR) := gd x s d z y -> gd x s d z y.
Parameter red blue: forall {x d z y}, @ADJ x d z y Phy.
Parameter good dull: forall {x d z y}, @ADJ x d z y Inf.
Parameter limbed: forall {x d z y}, @ADJ x d z y Lim.

Fail Check good him.  (*tests*)
Fail Check good he.
Check good john.
Check red: ADJ Phy: Type.
Check [red book]: STM _ ACC' _ _.
Check red book: STM _ NOM _ _.
Check good: ADJ Inf.
Check good book.
Check [good book].
Check red [good book].
Check [red [good book]].
Check good [red [good book]].
Check [book].
Check [good [good book]].
Check good [book].
Check limbed: ADJ Lim.
Fail Check red red.
Check red (red _).
Fail Check limbed book.
Fail Check [limbed book].
Check blue (red john).
Check blue (red book).
Check red [limbed john].
Fail Check red (limbed john).
Compute red (red john).
Check red (red (red book)).
Compute red book.
Fail Check limbed (red john).
Fail Check limbed (red book).
Check [limbed [red book]].
Check [limbed [red [book]]].
Check limbed (FF _ (red book)).
Check [red hut].
Check good boy: gs _ _ _ _ _.
Fail Check [red] [hut]. (*parsing*)
Fail Check red [limbed book].
Check red [limbed [book]].
Check limbed [red john].
Check red book.
Check [red book].
Check red john.
Check blue (red john).
Check FF _ (limbed (FF _ hut)).
Check good [red john].
Check good [red [limbed john]]. 
Fail Check limbed (red [limbed john]).
Check to john.
Check to (red john).
Check good john: XP0 _ _ _ PN+.


(*-------------------ADJECTIVAL ADVERB:-------------------*)

Definition A_ADV {x d z y s} := @ADJ x d z y s -> @ADJ x d z y s.
Parameter very: forall {x d z y s}, @A_ADV x d z y s.

Check very (very red) [limbed [blue john]].
Fail Check very (very red) limbed.
Fail Check very (very red) [limbed _].


(*-------------------GENERIC ADVERB:-------------------*)

Structure GADV := mkGADV {gadv: Type}.
Canonical Structure c_adj {x y z w v} := mkGADV (@ADJ x y z w v).           (*adjectival..*)
Canonical Structure c_vf {k l m n o}{x:Prop} := mkGADV (gp k l m n o -> x). (*verbial/flexibial..*)
Canonical Structure c_s := mkGADV S0.                                       (*sentential adverb*)
Definition G_ADV (x:GADV) := gadv x -> gadv x.
Parameter g_adv: forall {x}, G_ADV x.
Notation madly := g_adv.

Fail Check madly madly.
Fail Check madly hut.
Fail Check madly john.
Check madly (madly red) [limbed [john]].
Check madly (madly red) (red [limbed [john]]).
Check very (very (madly red)) [limbed [blue john]].
Check madly (very (very (madly red))) [limbed [blue john]].
Check red (red book).
Check red (red (book)).
Fail Check red (good book).
Check good [red [good book]].
Fail Check good book: _ Phy _ _.
Fail Check red book: _ Inf _ _.
Check good (good book).
Check blue [good [red book]].
Check red [good book].
Check to (madly (very (very (madly red))) [limbed [blue john]]).
Fail Check madly (very (very (madly red))) (to [limbed [blue john]]).
Check madly (very (very (madly red))) [good [blue book]].


(*-------------------DETERMINER:-------------------*)

Parameter the: forall {b:CXS}{x:SR}{d:CA}{z:NUM}{y}, gs b x d z y -> gp cxp_2 x d z y.
Parameter a: forall {b:CXS}{x:SR}{d w}, gs b x d SG w -> gp cxp_1 x d SG w.
Parameter this: forall {b:CXS}{x:SR}{d:CA}{w:ST}, gs b x d SG w -> gp cxp_2 x d SG w.
Parameter these: forall {b x d w}, gs b x d PLR w -> gp cxp_2 x d PLR w. (*cannot use XP.. (bec of con (also above))*)

Fail Check the the.
Check the book.
Fail Check the (the book).
Fail Check a (the book).
Check the ((madly red) [limbed [good john]]).
Fail Check these john.
Fail Check these book.
Fail Check red the.
Fail Check madly a.
Fail Check red (a hut).
Fail Check madly the.
Check a (very (very good) [red book]).
Check to (a book): _ Phy _ _.
Check to (the john).
Check to john.
Fail Check to (to john).
Fail Check the (to john).
Check to (the (very (very good) [red book])).
Check at (the (red book)).
Check the (good book).
Check at (the (good book)).
Check [[the (good book)]].
Check [the (good book)].
Check at (the book).
Check at (the thought).
Check at (the (good [red book])).
Check at (the (good book)).
Check @at' _ _ _ _ _ (the (good book)).
Check at [the (good book)]: _ -> _.
Check at [the (good book)]: XP2 _ _ _ _.
Check very red.
Fail Check a (very red).


(*-------------------PLURAL:-------------------*)

(*note: PL sends STM and XP1 to PLU*)
Parameter PL: forall {t:CXD}{f:SR}{d:CA}{z:ST}, gd t f d SG z -> gp cxp_p f d PLR z. (*cannot use PLU (bec of con)*)
Notation "-s" := PL (only parsing).

Check PL book.
Check PL book. Check PL(book).
Check -s book.
Check -s john.
Check these (-s john).
Check a john.
Check a book.
Fail Check a (-s john).
Check the ((madly (madly red)) [limbed [blue (-s john)]]).
Check the (very (madly (madly red)) [limbed [blue (-s john)]]).
Check XP1 _ _ _ _: Set.
Check STM _ _ _ _: Set.
Check to (the ((madly (madly red)) [limbed [blue (-s john)]])).
Check to (the ((madly (madly red)) (blue (-s book)))).
Check in (the (-s book)).
Check in [the (good [-s book])]: _ -> _.


(*-------------------QUANTIFIER:-------------------*)

Parameter all: forall {x f d z}, gqa x f d PLR z -> gp cxp_q2 f d PLR z.
Parameter every: forall {x f d z}, gqy x f d SG z -> gp cxp_q2 f d SG z.
(* Parameter entire: forall {x y f d z}, gqt y x f d z -> gp cxp_q1 x f d z. *)
Structure ENTIRE := mkENTIRE {enti:Type}.
Canonical Structure ent_sg {x z f} := mkENTIRE (gqt cxqt_s x f SG z -> gs cxs_q x f SG z). (*QU0 SG*)
Definition ent_pl' x z f := gqt cxqt_p x f PLR z -> gp cxp_q1 x f PLR z. (*fix for "redundant proj"*)
Canonical Structure ent_pl {x z f} := mkENTIRE (ent_pl' x z f). (*QU1*)
Definition ent_pl'' x z f := gqt cxqt_p x f PLR z -> gs cxs_q x f PLR z.
Canonical Structure ent_pl0 {x z f} := mkENTIRE (ent_pl'' x z f). (*QU0 PLR*)
Parameter entire': forall {x}, enti x. (*type inference for this fails (see below)*)
Notation "'entire'" := (@entire' _) (at level 200).
Notation "'entire' x" := ltac:(first [exact (@entire' ent_sg x) | exact (@entire' ent_pl x) | exact (@entire' ent_pl0 x)]) (at level 200).

Fail Check entire' hut.
Check entire hut.
Check entire: _ -> _.
Check (_: enti ent_sg) hut.
Check (_: enti ent_pl) (-s hut).
Check entire: _ -> _.
Check (entire: _ -> _) hut.
Check entire hut: gs _ _ _ _ _.
Check entire (-s hut): gp _ _ _ _ _.
Check every book.
Fail Check the (every book).
Fail Check a (every book).
Fail Check all (the book).
Check the (entire book).
Fail Check PL (entire book).
Fail Check entire john.
Fail Check every (a book).
Fail Check all (a book).
Check all (-s book).
Fail Check all book.
Fail Check all john.
Fail Check all (a book).
Fail Check all the.
Check all (the _).
Check all (the (red (-s book))).
Check all (very (very red) (blue (-s book))).
Fail Check all (all (red (blue (-s book)))).
Check in (all (very (very red) (blue (-s book)))).
Check in (all (the (-s hut))).
Fail Check in (all (the hut)).
Check in (every hut).
Check in (the (entire hut)).
Fail Check good (entire book).
Check the (entire (madly good book)).
Check a (entire book).
Check the (entire book).
Check these ((entire (-s thought)): gs _ _ _ _ _).
Check the (entire (-s ball)).
Check these (_ : gs _ _ _ _ _).
Check entire (-s thought): gs _ _ _ _ _.
Check entire (-s hut).
Check every ball.


(*-------------------TENSE-ASPECT-MOOD:-------------------*)

Parameter PAST PRES: forall {x:Type}, (NF -> x) -> x.


(*-------------------SENTENCE:-------------------*)

Parameter s0s1:> S0 -> S1.
Parameter s1s2:> S1 -> S2.
Notation S := S2.


(*-------------------VERB:-------------------*)

Parameter read: forall {b c:CXP}{z w:NUM}{y x f}, NF -> gp b Sen NOM z y -> gp c Inf (acc x) w f -> S0.
Check PAST read [a (good john)] [a (red book)].
Check PAST read [a (good john)] _.

Check a (red book).
Check [a (red book)].
Check good john.


(*-------------------FLEXIBLE:-------------------*)

(*simple*)

Structure Sleep := mSleep {sleep':Type}.
Canonical Structure sleep_s := mSleep (gs cxs_s Inf NOM SG F). (*need gs cxs_s instead of STM; all events are Inf*)
Canonical Structure sleep_d := mSleep (gd cxd_s Inf NOM SG F). (*req-ed for type inference only*)
Canonical Structure sleep_q := mSleep (gqy cxqy_s Inf NOM SG F). (*req-ed for type inference only: every sleep*)
Canonical Structure sleep_f {b:CXP}{v:NUM}{y} := mSleep (NF -> gp b Sen NOM v y -> S0).
Parameter sleep: forall {b:Sleep}, sleep' b.

Check sleep: NF -> _.
Check sleep: gs _ _ _ _ _.
Check @PAST: forall {x:Type}, (NF -> x) -> x.
Check john: gp _ _ _ _ _.
Check PAST sleep john: S.
Fail Check PAST sleep john: Prop.
Check a sleep: XP1 Inf NOM SG F.
Fail Check sleep: STM Inf NOM SG F.
Check good sleep.
Check the (madly good sleep).
Check PAST sleep [a (good john)].
Check PAST sleep [red (-s john)].
Check madly (madly (PAST sleep)) [madly (madly red) [limbed [blue (-s john)]]]: S0.
Check PAST sleep [madly (madly red) [limbed john]]: S0.
Check PAST sleep [madly (madly red) [limbed [blue (-s john)]]].
Check in (blue [good [-s book]]) (PAST sleep [madly (madly red) [limbed [blue (-s john)]]]).
Check at [the (good book)] ((PAST sleep) john).
Fail Check PAST sleep [to john].
Fail Check PAST sleep (a hut).
Check every sleep.

Structure Stone := mStone {stone':Type}.
Canonical Structure stone_s := mStone (gs cxs_s Phy (acc acc_n) SG F). (*need gs cxs_s instead of STM & (acc acc_n) instead of NOM*)
Canonical Structure stone_d := mStone (gd cxd_s Phy (acc acc_n) SG F). (*equiv to the above (for type inference only)*)
Canonical Structure stone_q := mStone (gqy cxqy_s Phy (acc acc_n) SG F). (*________''________*)
Canonical Structure stone_f {b c:CXP}{v u:NUM}{x f y} := mStone (NF -> gp b Lim NOM v x -> gp c Phy (acc f) u y -> S0).
Parameter stone: forall {b:Stone}, stone' b.
Existing Class Stone. Existing Instance stone_s. (*type inf: default instance*)

Check the (entire stone). (*requires default instance*)
Check the (blue stone).
Check PAST stone [a (good john)] (-s john): S0.
Check PAST stone [red (-s john)].
Check madly (madly (PAST stone)) [madly (madly red) [limbed [blue (-s john)]]] (the (-s hut)).
Check PAST stone [madly (madly red) [limbed john]] [good [-s hut]]: S0.
Check PAST stone [madly (madly red) [limbed [blue (-s john)]]].
Check in (blue [good [-s hut]]) (PAST stone [madly (madly red) [limbed [blue (-s john)]]] (-s book)).
Fail Check at [the (good book)] ((PAST stone) john).
Fail Check PAST stone [to john].
Fail Check PAST stone (a hut).
Fail Check good stone.
Check limbed [hut].
Check limbed [stone].
Check good [hut].
Fail Check good stone.
Compute good [stone]. (*requires default instance*)
Compute good [@stone stone_s].
Check red stone.
Fail Check limbed (red stone).
Check limbed [red stone].
Check red stone: _ Phy _ _ _.
Fail Check red stone: _ Inf _ _ _.
Check blue (red stone).
Check in (the stone).
Fail Check good stone.
Check red (red stone).
Check to (the stone).
Check these (-s stone).
Check every stone.
Check entire stone.
Check all (-s stone).
Fail Check all stone. (*interpreted as count noun only (mass/count distinction not implemented)*)

(*variadic*)

(*variadic func support (a generic ver would have {s:SR} instead of Phy)*)
Definition variad u v x := gp cxp_2 Phy u v x -> S0.
Parameter variad_S0:> forall u x y, variad u x y -> S0.

Structure Throw := mThrow {throw':Type}.
Canonical Structure throw_x := mThrow (gs cxs_s Inf NOM SG F).
Canonical Structure throw_d := mThrow (gd cxd_s Inf NOM SG F).
Canonical Structure throw_s {z w v x y f h}{b c:CXP} := mThrow (NF -> gp b Lim NOM z x -> gp c Phy (acc h) w y -> variad LAT v f).
Parameter throw: forall {b:Throw}, throw' b.

Check a (good throw).
Check PAST throw john (a book): S0.
Check PAST throw john him: S0.
Check PAST throw john (a book).
Check PAST throw john (a (entire book): gp _ _ _ _ _) (at john): S0.
Check PAST throw john (a book) (at john): S0.
Check PAST throw john (a book) (towards john): S0.
Check PAST throw john (a ball) (to john): S0.
Check PRES throw john (the (entire hut): gp _ _ _ _ _).
Check PRES throw john (-s stone).
Check PRES throw john (a stone) (at (a ((madly red) [good john]))).
Check PAST throw john (a book) (at (red (-s stone))): S0.
Check PRES throw [madly (madly red) [limbed john]] (a book).
Check PRES throw [madly (madly red) [limbed john]] (a ball).
Check PRES throw [madly (madly red) [limbed john]] him.
Check PRES throw [madly (madly red) [limbed john]] (a ((madly red) (blue [good book]))).
Check PRES throw [madly (madly red) [limbed [-s john]]].
Check PRES throw [madly (madly red) [limbed [-s john]]] (a ((madly red) (blue [good book]))).
Fail Check PRES throw [to john] (a book).
Fail Check PRES throw john (to (a book)).
Check madly (madly (PRES throw)) [the (madly (madly red) [limbed [good [-s john]]])] (all (these (red (-s book)))).

Structure Walk := mWalk {walk':Type}.
Canonical Structure walk_x := mWalk (gs cxs_s Inf NOM SG F).
Canonical Structure walk_d := mWalk (gd cxd_s Inf NOM SG F).
Canonical Structure walk_s {b:CXP}{z v x y} := mWalk (NF -> gp b Lim NOM z x -> variad DIR v y).
Parameter walk: forall {b:Walk}, walk' b.

Check PAST walk john.
Check PAST walk john: S0.
Check PAST walk john _: S0.
Check PRES walk john (towards (-s book)): S0.
Check a (good walk).
Check dull (-s walk).


(*-------------------SENTENTIAL ADVERB:-------------------*)

Parameter however: S0 -> S1.
Parameter importantly crucially: S1 -> S2.

Check however (PRES throw john (a stone) (at (a ((madly red) [good book])))): S.
Check importantly (however (PRES throw john (a ball) (to (the (very (madly (madly red)) [limbed [blue (-s john)]]))))): S.
Check importantly (however (PAST read [the (very (madly (madly red)) [limbed [blue (-s john)]])] [a ((madly red) (blue [good book]))])): S.
Check PAST read (all (-s john)) [a book].
Check PRES throw john (a book): S.
Check however (in (-s hut) (PAST walk john (towards (the (-s book))))): S.
Check crucially (in (the hut) (PAST walk john)): S.
Fail Check crucially (in (the hut) ((PAST walk john) (in (a hut)))): S.
Check crucially (in (the hut) ((in (a hut)) ((PAST walk john)))): S. (*Crucially, john walked in a hut in the hut.*)
Check crucially (in (the hut) (madly (PAST walk john) (to (a hut)))): S. (*Crucially, john walked madly to a hut in the hut.*)
Check crucially (in (the hut) (madly (PAST walk) john (to (a hut)))): S. (*Crucially, john walked madly to a hut in the hut.*)
Check crucially (in (all (the (-s hut))) (madly (PAST walk) john (to (a hut)))): S. (*Crucially, john walked madly to a hut in all the huts.*)
Check crucially (madly (in (the hut) (madly (PAST walk john) (to (a hut))))). (*Crucially, madly, john walked madly to a hut in the hut.*)
Fail Check madly (crucially (in (the hut) (madly (PAST walk john) (to (a hut))))). (*Madly, crucially, john walked madly to a hut in the hut.*)
Check the (good book): XP2 _ NOM _ _.
Check PRES throw john (a stone): _ -> _.
Fail Check PRES throw john (a stone) (at (the (good book))).
Check PRES throw john (a stone) [at (the (good book))].
Check PRES throw john (a ball) [at (the (good book))].
Check however (PRES throw john (a stone) (at john)): S.
Check importantly (PRES throw john (a stone) _): S.
Fail Check importantly (importantly (PRES throw john (-s stone))).
Fail Check however (importantly (PRES throw john (a ball))).
Fail Check however (however (PRES throw john (a stone))).


(*-------------------CON:-------------------*)

(*default terms*)
Parameter ca:CA.
(* Parameter sr:SR. *)
Parameter num:NUM.
Parameter st:ST.
Parameter cxd':CXD.

(*con type inf: default instances*)
Existing Class CA. Existing Instance ca.
Existing Class NUM. Existing Instance num.
Existing Class ST. Existing Instance st.
Existing Class CXD. Existing Instance cxd'.
Existing Class SR. Existing Instance Phy.

Scheme Equality for CA. (*for CA_beq*)
Scheme Equality for SR. (*for SR_beq*)

(*---sentential con:---*)

Parameter s_base: S0 -> S0 -> S0. (*cannot use S*)

(*---nominal con:---*)

Parameter x_base: forall {b c s t k l o p x y}, gp b s k o x -> gp c t l p y ->
(if CA_beq k l then (if SR_beq s t then gp cxp_2 s k num st else gp cxp_2 Phy k num st)
else (if SR_beq s t then gp cxp_2 s ca num st else gp cxp_2 Phy ca num st)).

(*---adjectival con:---*)

Parameter aa: forall {b c s t k l o p x y e f g h i}, @ADJ b s k x o -> @ADJ c t l y p -> @ADJ e f g h i.
Parameter adj_base: forall {b c s t k l o p x y e f g h}, @ADJ b s k x o -> @ADJ c t l y p ->
(if SR_beq o p then @ADJ e f g h p else @ADJ e f g h (sr0 o p)).

(*---adverbial con:---*)

Parameter gadv_base: forall x, G_ADV x -> G_ADV x -> G_ADV x.

(*---all con-s:---*)

Structure CON := mCON {dom_con:Type; ran_con:Type; con: dom_con -> ran_con}.
Canonical Structure con_s := mCON _ _ s_base. (*S0*)
Canonical Structure con_variad {x} := mCON (variad x _ _) _ s_base. (*variad-as-S0*)
Definition x_fix {c s t k l p y} := gp c t l p y -> (*fix for "redundant proj" warning*)
(if CA_beq k l then (if SR_beq s t then gp cxp_2 s k num st else gp cxp_2 Phy k num st)
else (if SR_beq s t then gp cxp_2 s ca num st else gp cxp_2 Phy ca num st)).
Canonical Structure con_x {b c s t k l o p x y} := mCON _ x_fix (@x_base b c s t k l o p x y). (*gp..*)
Definition a_fix {c t l y p o e f g h} := @ADJ c t l y p -> (if SR_beq o p then @ADJ e f g h p else @ADJ e f g h (sr0 o p)).
Canonical Structure con_a {b c s t k l o p x y e f g h} := mCON _ a_fix (@adj_base b c s t k l o p x y e f g h). (*ADJ*)
Definition g_fix {x} := G_ADV x -> G_ADV x.
Canonical Structure con_ga {x} := mCON _ g_fix (gadv_base x). (*G_ADV*)

Check con _ madly madly.
Check con _ red red.
Check con _ (PAST sleep john) (PAST sleep john). (*checks bec we set S0 instead of S*)
Check con _ (a book) (a book). (*checks bec we generalized from (PLU|XP(1|2)) to gp*)
Check con _ (john:gp _ _ _ _ _) john.
Fail Check con _ [john] john.
Check con _ john john.
Check con _ (a book) john.
Check con _ john john.
Check con con_x (-s book) (a stone).
Check con con_x (a sleep) (-s sleep).
Check con con_a _ _.
Check con con_a _ _.

Notation "'and'" := (con _) (at level 8). (*level req-ed (all not-s of "and" must be on the same level)*)
Notation "'and' x y" := (con _ x y) (at level 8, x, y at level 9).

Check and.
Fail Check and _ _.
Check and john (a book).
Check and red limbed.
Check madly (and red limbed).
Fail Check and _ _.
Check madly (and red limbed).
Check madly (con (_:CON) red limbed).
Check madly (con (mCON _ _ _) red limbed).
Fail Check and john (PAST sleep john).
Check and madly madly red.
Check and john john.
Fail Check and [john] john.
Check and (a book) john.
Check and (john:gp _ _ _ _ _) john.
Fail Check and (john:XP1 _ _ _ _) john.
Check and (john:gp _ _ _ _ _) john.
Check and (a hut:gp _ _ _ _ _) (a hut).
Check and (-s hut) (-s hut).
Check and john (-s ball).
Check and john john.
Check and (a hut:gp _ _ _ _ _) (a hut).
Check and red limbed.
Fail Check and red (_:S).
Fail Check and (_:S) (_:ga _ _ _ _ _).
Fail Check and john (PAST sleep john).
Fail Check and hut (PAST sleep john).

Check @g_adv c_adj (con _ red red) john.
Check @g_adv c_adj (con _ red limbed) [john].
Check @g_adv c_adj (_ _ red limbed) john.
Check gadv_base c_adj _ _ (gadv_base _ _ _ (con _ red red)) hut.
Check gadv_base c_adj _ _ (gadv_base _ _ _ (con _ red limbed)) [hut].
Check gadv_base c_adj _ _ (gadv_base _ _ _ (_ _ red limbed)) hut.
Check @g_adv c_adj (_ _ red limbed) hut.
Check @g_adv c_adj _ john.
Check @g_adv c_adj _ john.
Check @g_adv c_adj _ hut.
Check gadv_base c_adj g_adv g_adv ((gadv_base c_adj g_adv g_adv) (con _ red red)) john.
Check gadv_base c_adj g_adv g_adv _ john.
Check gadv_base c_adj _ _ _ john.
Check gadv_base c_adj _ _ _ hut.
Check gadv_base c_adj _ _ (_ (con _ red red)) hut.
Check gadv_base c_adj _ _ (_ (con _ red limbed)) hut.

(*---ad(j|v) con:---*)

Ltac adj_not x y z := (refine ((@aa _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x y) z)). (*need refine; using adj_base fails*)

Check ltac:(refine ((@aa _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) hut)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) [hut])).
Fail Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) hut)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) [hut])).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red limbed) [john])).
Fail Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) john)).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ limbed red) [john])).
Check ltac:(refine ((@adj_base _ _ _ _ _ _ _ _ _ _ _ _ _ _ red red) hut)).

Notation and_abs x y z := ltac:(lazymatch type of x with
  | G_ADV _ => lazymatch type of y with (*adv*)
      | G_ADV _ => refine (gadv_base _ (@g_adv _) (@g_adv _) z) end
  | ADJ ?k => lazymatch type of y with (*adj*)
      | ADJ ?l => let u := constr:(z: ga _ _ _ _ _) in lazymatch type of u with (*should i use gs?*)
        | ga _ ?m _ _ _ => lazymatch z with
          | @FF _ _ _ _ _ _ _ => adj_not x y ([z]: ga _ l _ _ _)
          (*must match 2nd arg-s sr (per con_a); using @F(F|gp) and ga _ _.. fails*)
          | _ => tryif unify k m then (tryif unify l m then adj_not x y z
            else idtac "err adj"; fail) else idtac "err adj0"; fail
        | _ => idtac "err adj1"; fail end end end
  | _ => idtac "uncaught"; fail end).

Check madly (con (_:CON) red limbed).
Check madly (con (mCON _ _ _) red limbed).
Check limbed [red hut].
Check and red red _.
Check and madly madly limbed john.
Check and madly madly limbed [red hut].
Check and madly madly limbed [red john].

Notation "'and' x y z" := (and_abs x y z) (at level 8, x, y, z at level 9).

Check and_abs red limbed [john].
Check and red limbed [john].
Check (and red red) ((and red red) _).
Check red (red _).
Check and red limbed.
Check (and red limbed) [book].
Check madly red [and red good [book]].
Check madly red [(and red good) [book]].
Check (and madly madly) (and red red) hut.
Check madly ((and madly madly) (and red red)) hut.
Check madly ((and madly madly) (madly (and red red))) hut.
Check madly ((and madly madly) (madly red)) ((and red red) (red hut)).
Check madly ((and madly madly) ((and madly madly) (madly (madly red)))) (red ((and red red) ((and red red) (red hut)))).
Check madly ((and madly madly) (madly (and limbed red))) [hut].
Check (and madly madly) (and red red) ((and red red) hut).
Fail Check (and madly madly) (and red limbed) hut.
Check (and madly madly) (and red limbed) [hut].
Check and red red.
Check (and red red) hut.
Check red ((and red red) hut).
Check and red limbed.
Fail Check and red limbed john.
Check (and red limbed) [john].
Check madly (and red red) _.
Check john.
Check (and madly madly) (and red limbed).
Check madly (con (_:CON) red limbed).
Check madly (and red limbed).
Check madly red (red _).
Check madly limbed [(and red limbed) [john]].
Fail Check madly limbed (and red limbed (blue [john])).
Check madly limbed (and red limbed [blue [john]]).
Check madly (and red limbed) [john].
Check (and madly madly) (PAST read).
Check (and madly madly) (PAST sleep john).
Check (and madly madly) limbed john.
Check (and madly madly) limbed [red john].
Check (and madly madly) limbed [red hut].
Check (and madly madly) (and red red) john.
Check madly (and red limbed) [hut].
Check (and madly madly) (and red limbed) [john].
Check (and madly madly) (and red limbed) [red john].
Check (and madly madly) (and red limbed) [red [hut]].
Check and_abs blue limbed ([john]: gs _ _ _ _ _).
Fail Check and_abs blue limbed john.
Check and_abs blue limbed [john].
Check and_abs blue red john.
Check and (PAST sleep john) (PAST sleep john).
Check and limbed red.
Check and red red hut.
Fail Check and limbed red john.
Check and limbed red [john].
Check and red limbed [john].
Check (and blue limbed) [john].
Fail Check and blue limbed john.
Check and red red (-s john).
Check red and red red (-s john).
Check red (red and red red (-s john)).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red and limbed red [hut]))).
Check the (red (red (red and limbed red [-s john]))).
Check (and (@red _ _ SG N) (@red _ _ SG N)) hut.
Check (and red red) hut.
Check (and red red) john.
Check (and red red) (-s john).
Check red ((and red red) (-s john)).
Check red (red and red red (-s john)).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red (and limbed red [hut])))).
Check the (red (red (red (and limbed red [-s john])))).
Fail Check a (red (red (red (and limbed red) [-s john]))).
Fail Check (and madly madly) (and red limbed) john.
Check (and madly madly) (and red limbed) [john].
Fail Check red (red (red [and blue limbed john])).
Check red (red (red [and blue limbed [john]])).
Check red (red (red ((and blue red) (-s john)))).
Check a (red (red (red ((and blue red) john)))).
Check a (red (red (red (and limbed red [john])))).
Check red (red ((and blue red) hut)).
Check red (red (red ((and blue red) hut))).
Check red (red (red ((and blue red) john))).
Check red (red (red ((and blue red) (-s hut)))).
Check a (red (red (red ((and blue red) hut)))).
Check a (red (red (red [and blue limbed [hut]]))).
Check a (red (red (red ((and blue red) john)))).
Check red (red (red [and blue limbed [-s john]])).
Check red (red (red (and blue red (-s john)))).
Fail Check a (red (red (red ((and blue red) (-s john))))).
Check a (red (red (red [and blue limbed [john]]))).
Check red (red (red [and blue limbed [-s john]])).
Check (and red red) john.
Check (and red red) (-s john).
Check red (and red red (-s john)).
Check red (red (and red red (-s john))).
Check red (red (red and red red (-s john))).
Check red (red (red and red red (-s hut))).
Check a (red (red (red and limbed red [hut]))).
Check the (red (red (red and limbed red [-s john]))).
Check [red].
Check [and blue red].
Check and blue red.
Check red (and blue red hut).
Check red (red and blue red hut).
Check red (red (and blue red hut)).
Check red (red ((and blue red) hut)).
Check red (red [and blue limbed [hut]]).
Check a (red ((and blue red) hut)).
Check a (red (red ((and blue red) hut))).
Check red (red (red ((and blue red) hut))).
Check red (red (red (red ((and blue red) hut)))).
Check red (red ((and blue red) (-s hut))).
Check red (red ((and blue red) (-s hut))).
Fail Check a (red ((and blue red) (-s hut))).
Check a (red ((and blue red) john)).
Check (red ((and blue red) hut)).
Check (and blue red) john.
Check a ((and blue red) john).
Check (and blue red) hut.
Check (and blue red) john.
Check PAST throw john (and (a stone) (-s stone)).
Check at (a ((and blue red) hut)).
Check at (a (and blue red hut)).
Check at (a [(and blue red) hut]).
Check (and blue red) john.
Fail Check [and blue red] hut.
Check (and blue red) hut.
Check and blue red hut.
Check [and blue red hut].
Check [(and blue red) hut].
Check _ _ red hut.
Check and (a stone) (-s stone).
Check and (-s book) (a ball).
Check PAST throw john (and (a stone) (-s stone)).
Check PAST throw john [and john (-s ball)].
Check PAST throw john (and (all (-s john)) (-s ball)).
Check PAST throw john (and john (-s ball)).
Check PAST throw john [and (-s ball) john].
Check PAST throw john (and (-s ball) john).
Check and (-s book) (-s ball).
Check and (-s book) [-s ball].
Fail Check and [-s book] (-s ball). (*per x_base and x_fix, and's 1st arg's type must be gp..*)
Check and ([-s book]: gp _ _ _ _ _) (-s ball).
Check PAST throw john [and (-s book) (-s ball)].
Check PAST throw (every john) (and (-s book) (-s ball)).
Check PAST throw john (and (-s book) [-s ball]).
Check PAST throw john ((and (-s book) ([-s ball]: gp _ Phy _ _ _))).
Fail Check PAST throw john (and [-s book] (-s ball)). (*and's 1st arg's type must be gp..*)
Check PAST throw john (and ([-s book]: gp _ _ _ _ _) (-s ball)).
Check and (a sleep) (-s sleep).
Check and (PAST walk john) (PAST walk john).
Check and (PAST walk john _) (PAST walk john).
Fail Check and (crucially (however (PAST sleep john))) (PAST sleep [-s john]).
Check crucially (however (and (PAST sleep (every john)) (PAST sleep [-s john]))).
Check and (a walk) (-s walk).
Check and (PAST throw john (a ball)) (PAST throw john (-s stone)).
Check and (PAST throw john (a ball) _) (PAST throw john (-s stone)).
Fail Check and (PAST throw john (a ball) _ _) (PAST throw john (-s stone)).
Check and (variad_S0 _ _ _ (PAST throw john (a ball))) (PAST throw john (-s stone)).
Check and (PAST throw john (a stone) (at (the hut))) (PAST throw john (-s stone)).
Fail Check and (PAST throw john (a stone) (at (the hut)))
(PAST throw [red (red (red and blue limbed [-s john]))] (-s stone)).
Check and (PAST throw john (a stone) (at (the hut)))
(PAST throw [red (red (red [and blue limbed [-s john]]))] (-s stone)).
Fail Check and (PAST throw john (a stone) (at (the hut))) [red limbed [-s john]].
Fail Check and (PAST throw john (a stone) (at (the hut))) [red (limbed [-s john])].
Fail Check and (PAST throw john (a stone) (at (the hut))) [red [limbed [-s john]]].

(*The following examples are generously commented and should be self-explanatory*)

Check PAST throw john. (*"John threw" type checks..*)
Fail Check PAST throw john: S. (*..but not as a sentence.*)
Check PAST throw john (-s stone): S.
Check PAST throw john (-s stone) (at (the hut)): S.
(*"At the hut" can be the 3rd argument of "throw" (above)..*)
Fail Check PAST throw john (at (the hut)). (*..but not the 2nd one.*)
Fail Check PAST throw (a hut). (*"A hut" cannot be the 1st argument of "throw"..*)
Check PAST throw [a hut]. (*..except in brackets (this works in Lax mode only).*)
Fail Check PAST throw john (-s stone) (in (the hut)).
(*"In the hut" cannot be an argument of "throw"..*)
Check in (the hut) (PAST throw john (-s stone)): S. (*..but it can be a sentence modifier..*)
Check at (every hut) (PAST throw john (-s stone)): S. (*..and so can "at every hut"..*)
Check however (PAST throw john (-s stone)): S. (*..and sentential adverbs like "however".*)
Fail Check and (PAST throw john (a stone)) john.
(*Connectives cannot range over a sentential and nominal argument..*)
Check and (every john) (all (the (-s boy))). (*..but can range over nominal..*)
Check and (PAST walk (-s boy) (to (all (the (-s hut))))) (PAST sleep john). (*..or sentential arguments..*)
Check and (PAST walk (-s boy) (to (all (-s hut)))) (PAST sleep john). (*..or sentential arguments..*)
Check and (PAST walk john) (PAST sleep john). (*..(also w/ optional arguments omitted).*)
Check PAST walk [and (every john) (all (the (-s boy)))] (to (the hut)).
(*every john and all the boys walked to the hut*)
Fail Check in (all (the hut)). (*ungrammatical?*)
Check in (the (entire hut)).
Check madly ((in (the (entire hut))) ((PAST walk) (all (the (-s boy))))): S.
(*all the boys walked madly in the entire hut*)

(*Examples of intersection types:*)
Check john: XP0 (hu _) NOM SG (Pn Ml).
Check john: XP0 (hu H_Phy) NOM SG (Pn Ml).
Check john: XP0 Phy NOM SG (Pn Ml).
Check limbed john: XP0 Lim NOM SG PN+.
Fail Check limbed john: XP0 Phy NOM SG (Pn Ml).
Fail Check limbed (john: XP0 Phy NOM SG (Pn Ml)).
(*"John" is an XP, human, proper name, male, in nominative, singular, a physical entity..*)
Check john: XP0 (hu H_Lim) NOM SG PN+. (*..and a limbed entity.*)
Check john: XP0 Lim NOM SG (Pn Ml). (*The normalized version of the previous type.*)
Check the john: XP2 _ NOM SG (Pn Ml). (*"The John" is another type of XP.*)
Check and (a (good throw)) (entire (-s throw)).
(*"Throw" is a flexible (a function or argument (as above))..*)
Check and (PRES stone john (a boy)) (PAST throw john (-s stone) ((at (-s stone)))).
(*..and "stone" likewise.*)
(*So "at stones" is an XP, flexible in lative, plural and a physical entity:*)
Check at (-s stone): XP2 Phy LAT PLR F.
(*As prescribed by "red", "red John" is a physical entity:*)
Check red john: XP0 Phy NOM SG (Pn Ml).
Check [red john]: XP0 Lim NOM SG PN+. (*"PN+" means proper name w/ gender info*)
(*..but we can make it into a limbed one w/ our bracket notation (above).*)
Check hut: STM Phy _ _ _. (*"Hut" is a stem and always a physical entity..*)
Check [hut]: STM Lim _ _ _. (*..so we can make it into a limbed one only in Lax mode..*)
(*..(the above line fails in Strict mode).*)
Check and (the (entire hut)) (all (-s john)): XP2 Phy NOM _ _. (*"All Johns and the entire hut" is an XP and physical entity in nominative..*)
Check and (the (entire hut)) (all (-s john)): XP2 Phy ACC' _ _. (*..or pseudo-accusative (by zero-derivation)..*)
Check [and (the (entire hut)) (all (-s john))]: XP2 Sen ACC' _ _. (*..which can be made into a sentient entity in Lax mode only*)

(*"John threw madly blue stones at the hut and red limbed boys." has 2 parses:*)
Check madly (PAST throw) john (blue (-s stone)) (at (and (the hut) (red [limbed (-s boy)]))): S.
Check PAST throw john (madly blue (-s stone)) (at (and (the hut) (red [limbed (-s boy)]))): S.
(*Here we used "[...]" to make a limbed entity into a physical one.*)
(*Arguments of verbs and flexibles must be proper XPs:*)
Fail Check PAST throw (the boy) (entire stone) (to him).
Check PAST throw (the boy) (the (entire stone)) (to him): S.
(*Arguments of V and F must have correct cases and selectional restrictions:*)
Check PAST throw he (all (-s stone)) (at john): S.
Check PAST throw john (all (the (-s stone))) (at him): S.
Check PRES throw (all (the (-s boy))) (every ball) (to him): S.
Fail Check PAST throw john (all (-s stone)) (at he). (*wrong case*)
Fail Check PAST throw him (all (-s stone)) (at john). (*wrong case*)
Fail Check PRES throw john (a walk). (*wrong case and selectional restriction*)
Fail Check PRES throw john [a walk]. (*wrong case?*)
Fail Check [a walk]: _ _ (acc _) _ _.
Check [a walk]: _ _ ACC' _ _.
Fail Check PRES throw (a walk) john. (*wrong selectional restriction*)
Check PRES throw [a walk] john. (*succeeds in Lax mode only*)
Check PRES throw john (-s stone) (at (all (madly (madly red) (red (-s hut))))): S.
Check PRES throw john (-s stone) (at (all (madly (madly red) (red [limbed [-s hut]])))): S.
(*We can stack adverbs and adjectives (note that the above line fails in Strict mode)..*)
Fail Check PRES throw john (-s stone) (at (madly madly red (red [limbed [-s hut]]))).
Fail Check PRES throw john (-s stone) (at (madly (madly (red (red [limbed [-s hut]]))))).
(*..but must be careful w/ parentheses.*)

Check all ((and madly madly) red (red (red (and blue red (-s john))))): QU2 _ _ _ _.
Check all ((and madly madly) red (red (red [and blue limbed [-s john]]))).
Check all ((and madly madly) red (red [and blue limbed [-s john]])).
(*Adjectival and adverbial connectives are supported..*)
Fail Check all (and madly madly red (red (red [and blue limbed [-s john]]))).
(*..but again we must be careful w/ parentheses.*)
Check red [limbed john].

(*---------------TRUTH-FUNCTIONALITY (OPTIONAL):------------*)

Module UNDec. (* all S-s undecidable *)
Parameter s_prop:> S -> Prop.
End UNDec.

Module TRue. (* all S-s True *)
Coercion s_prop (x:S) := True.
End TRue.

Module NonTriv. (* a nontrivial model *)
Parameter s_prop:> S -> Prop.
Notation ":>> x" := (s_prop (s1s2 (s0s1 x))) (at level 179).
Notation "$ x" := ltac:(let u := constr:(x:Prop) in lazymatch u with
  | :>> (PAST _ _) => exact True
  | s_prop (s1s2 (however (PAST _ _))) => exact True
  | :>> (PRES sleep john) => exact True
  | :>> (variad_S0 _ _ _ (PRES walk (-s boy))) => exact True
  | :>> (PRES _ _) => exact False
  | :>> (variad_S0 _ _ _ (PRES _ _)) => exact False
  | :>> (and (PAST _ _) (PAST _ _)) => exact True (*sleep..*)
  | :>> (and (PAST _ _) (variad_S0 _ _ _ (PAST _ _))) => exact True (*walk..*)
  | :>> (and (PAST _ _) (PRES sleep john)) => exact True
  | :>> (and (PAST _ _) (variad_S0 _ _ _ (PRES walk (-s boy)))) => exact True
  | :>> (and ?m ?n) => idtac "unanalyzed:" m n
  | s_prop (s1s2 (however ?n)) => idtac "unanalyzed:" n
  | x => idtac "unanalyzed:" x": Prop" (*leave some potentially t-functional constructs unanalyzed*)
  end) (at level 199).
End NonTriv.

(*Model switches (comment out at will)*)
(* Import UNDec. (*highest priority if multiple on *) *)
(* Import TRue. *)
Import NonTriv. (*lowest priority if multiple on *)

Check PAST sleep john: Prop.
Check and (PAST sleep john) (PAST walk john): Prop.
Compute PRES sleep john: Prop.
Check $(PRES walk john): Prop. (*False*)
Fail Check $(PAST walk stone). (*type mismatch*)
Check $(PRES sleep john). (*True*)
Check $(PRES sleep (PL boy)). (*False*)
Check $(PAST sleep (-s boy)). (*True*)
Fail Check $sleep. (*type inference fail*)
Fail Check $john. (*type mismatch*)
Fail Check $(PAST _ _). (*type inference fail*)
Check $(and (PAST walk john) (PAST walk john)): Prop. (*True*)
Check $(and (PAST walk john) (PAST sleep john)). (*True*)
Check $(and (PAST sleep john) (PRES sleep john)). (*True*)
Check $(and (PAST sleep john) (PRES walk john)). (*unanalyzed:..*)
Check $(and (PAST sleep john) (PRES walk (-s boy))). (*True*)
Check $(however (PAST sleep john)). (*True*)
Check $(however (PRES sleep john)). (*unanalyzed:..*)
Check $(1=2). (*unanalyzed: (1 = 2): Prop*)
Check $(forall x y:unit, x=y). (*unanalyzed.. Prop*)
Fail Check $Type. (*type mismatch*)

(*a trivial proof that "boys don't sleep" and "john sleeps"*)
Theorem pres: (~ ($ (PRES sleep (-s boy)))) /\ ($ (PRES sleep john)). Proof. firstorder. Qed.

(*l6pp*)